﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

	public int width;
	public int height;

	public string seed;
	public bool useRandomSeed;

	[Range(0,100)]
	public int randomFillPercent;

	[Range(1,10)]
	public int borderSize;

	int[,] map;

	void Start() {
		GenerateMap();
	}

	void Update() {
		if (Input.GetMouseButtonDown(0)) {
			GenerateMap();
		}
	}

	void GenerateMap() {
		map = new int[width, height];
		RandomFillMap();

		for (int i = 0; i < 5; i++) {
			SmoothMap();
		}
			
		int[,] borderedMap = new int[width + borderSize * 2, height + borderSize * 2];

		for (int x = 0; x < borderedMap.GetLength(0); x++) {
			for (int y = 0; y < borderedMap.GetLength(1); y++) {
				if ((x >= borderSize && x < width + borderSize) && (y >= borderSize && y < height + borderSize)) {
					borderedMap[x, y] = map[x - borderSize, y - borderSize];
				}
				else {
					borderedMap[x, y] = 1;
				}
			}
		}

		MeshGenerator meshGen = GetComponent<MeshGenerator>();
		meshGen.GenerateMesh(borderedMap, 1f);
	}

	void RandomFillMap() {
		if (useRandomSeed) {
			seed = Time.time.ToString();
		}

		System.Random pseudoRandom = new System.Random(seed.GetHashCode());

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
					map[x, y] = 1;
				}
				else {
					map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;
				}
			}
		}
	}

	void SmoothMap() {
		int[,] tempMap = new int[width,height];
		/* copy current map into temp to prevent bias */
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				tempMap[x, y] = map[x, y];
			}
		}


		for (int x = 1; x < width-1; x++) {
			for (int y = 1; y < height-1; y++) {
				int neighborWallTiles = GetSurroundingWallCount(x, y);

				if (neighborWallTiles > 4)
					tempMap[x, y] = 1;
				else if (neighborWallTiles < 4)
					tempMap[x, y] = 0;
			}
		}


		/* copy temp back into maps */
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				map[x, y] = tempMap[x, y];
			}
		}
	}

	int GetSurroundingWallCount(int gridX, int gridY) {
		int wallCount = 0;
		for (int nX = gridX-1; nX <= gridX+1; nX++) {
			for (int nY = gridY - 1; nY <= gridY + 1; nY++) {	
				// Skip self
				if (nX == gridX && nY == gridY)
					continue;
				// Bounds check
				if (nX < 0 || nX >= width || nY < 0 || nY >= height) {
					// But increment walls so that we can encourage growth
					wallCount++;
					continue;
				}
				wallCount += map[nX, nY];
			}
		}
		return wallCount;
	}

	/*void OnDrawGizmos() {
		if (map != null) {
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					Gizmos.color = (map[x,y] == 1) ? Color.black : Color.white;
					Vector3 pos = new Vector3(-width / 2 + x + 0.5f, 0, -height / 2 + y + 0.5f);
					Gizmos.DrawCube(pos, Vector3.one);
				}
			}
		}
	}*/
}
